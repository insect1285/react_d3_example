import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import * as d3 from 'd3';
import logo from './logo.svg';
import './App.css';


const genRandomTree = (N = 300) => {
    return {
        nodes: [...Array(N).keys()].map(i => ({id: i})),
        links: [...Array(N).keys()]
            .filter(id => id)
            .map((id, i) => ({
                id: i,
                source: id,
                target: Math.round(Math.random() * (id - 1)),
            }))
    };
};

d3.scaleOrdinal(d3.schemeCategory10);

class force {
    constructor(nodes, links) {
        this.nodes = nodes;
        this.links = links;
        this.width = 300;
        this.height = 600;

        this.initForce();
    }


    initForce = () => {
        this.force = d3.forceSimulation(this.nodes)
            .force("charge", d3.forceManyBody())
            .force("link", d3.forceLink().links(this.links).distance(5).strength(1))
            .force("center", d3.forceCenter(this.width / 2, this.height / 2))
            .force("collide", d3.forceCollide([5]).iterations([5]))
            .alphaDecay(0.02)
            .velocityDecay(0.1)
    };

    updateNode = (node) => {
        node
            .attr("transform", (d) => "translate(" + d.x + "," + d.y + ")")
    };

    updateLink = (link) => {
        link
            .attr('d', d => `M ${d.source.x}, ${d.source.y} L ${d.target.x}, ${d.target.y}`)
    };


    updateGraph = (graph) => {
        graph.selectAll('.node')
            .call(this.updateNode);
        graph.selectAll('.link')
            .call(this.updateLink);
    };

    dragStarted = (d) => {
        if (!d3.event.active) this.force.alphaTarget(0.3).restart();
        d.fx = d.x;
        d.fy = d.y
    };

    dragging = (d) => {
        d.fx = d3.event.x;
        d.fy = d3.event.y;
    };

    dragEnded = (d) => {
        if (!d3.event.active) this.force.alphaTarget(0);
        d.fx = null;
        d.fy = null
    };

    drag = (node) => d3.select(node)
        .call(d3.drag()
            .on("start", this.dragStarted)
            .on("drag", this.dragging)
            .on("end", this.dragEnded)
        );

    tick = (that) => {
        console.log('tick');
        const canvas = d3.select(that);
        this.force.on('tick', () => {
            canvas.call(this.updateGraph)
        });
    };

    zoom = (that) => {
        const canvas = d3.select(that);
        canvas.call(
            d3.zoom()
                .scaleExtent([0.00001, 10000000])
                .on("zoom", this.zoomed)
        )
        .on("dblclick.zoom", null);
    };

    zoomed = () => {
        d3.selectAll('g[id="links"]').attr('transform', d3.event.transform);
        d3.selectAll('g[id="nodes"]').attr('transform', d3.event.transform);
    };
}

class App extends Component {
    constructor(props) {
        super(props);
        const {nodes, links} = genRandomTree(100);
        this.state = {
            nodes: nodes,
            links: links,
        };
        // Initialize an object with initial methods
        this.FORCE = new force(nodes, links);
        this.addNode = this.addNode.bind(this);
        this.svgRef = React.createRef();
    }

    componentDidMount() {
        const {nodes, links} = this.state;
        this.FORCE.initForce(nodes, links);
        this.FORCE.tick(this.svgRef.current);
        this.FORCE.zoom(this.svgRef.current);
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.nodes !== this.state.nodes || prevState.links !== this.state.links) {
            const {nodes, links} = this.state;
            this.FORCE.force.nodes(nodes);
            this.FORCE.force.force("link").links(links);
        }
    }

    addNode(nodeData) {
        this.setState(prevState => ({
            nodes: [...prevState.nodes, {id: prevState.nodes.length,}],
            links: [...prevState.links, {
                id: prevState.links.length,
                source: nodeData.id,
                target: prevState.nodes.length,
                numParticles: 1,
            }]
        }));
    }

    render() {

        const links = this.state.links.map((link) => {
            return (
                <Link
                    key={link.id}
                    data={link}
                    force={this.FORCE}
                />);
        });

        const nodes = this.state.nodes.map((node) => {
            return (
                <Node
                    data={node}
                    key={node.id}
                    addNode={this.addNode}
                    force={this.FORCE}
                />);
        });

        const width = window.innerWidth;
        const height = window.innerHeight;

        return (
            <div>
                <p>Click on a node to attach a new node to it</p>
                <div className={'chart'}>
                    <svg
                        width={'100%'}
                        height={'100%'}
                        viewBox={`0,0,${Math.min(width, height)},${Math.min(width, height)}`}
                        preserveAspectRatio={'xMidYMid meet'}
                        ref={this.svgRef}
                    >
                        <g id={'links'}>
                            {links}
                        </g>
                        <g id={'nodes'}>
                            {nodes}
                        </g>
                    </svg>
                </div>
            </div>
        );
    }
}

class Link extends Component {

    componentDidMount() {
        this.d3Link = d3.select(ReactDOM.findDOMNode(this))
            .datum(this.props.data)
    }

    componentDidUpdate() {
        this.d3Link.datum(this.props.data)
            .call(this.props.force.updateLink);
    }

    render() {
        return (
            <path
                className='link'
                stroke={'gray'}
                strokeWidth={1}
                opacity={0.8}
                onMouseOver={() => console.log('mouse over link ' + this.props.data.id)}
            />
        );
    }
}

class Node extends Component {

    componentDidMount() {
        this.d3Node = d3.select(this.nodeRef.current)
            .datum(this.props.data);
        this.props.force.drag(this.nodeRef.current);
    }

    componentDidUpdate() {
        this.d3Node.datum(this.props.data)
            .call(this.props.force.updateNode)
    }

    nodeRef = React.createRef();

    render() {
        return (
            <g
                className='node'
                transform={`translate(${this.props.data.x || 0},${this.props.data.y || 0})`}
                onClick={() => this.props.addNode(this.props.data)}
                onDoubleClick={() => console.log('Double Clicked')}
                onMouseOver={() => console.log('mouse over node ' + this.props.data.id)}
                ref={this.nodeRef}
            >
                <image
                    xlinkHref={logo}
                    width={50}
                    height={50}
                    x={-25}
                    y={-25}
                >
                </image>
                <text
                    textAnchor={'middle'}
                    alignmentBaseline={'middle'}
                    fontSize={10}
                >
                    {this.props.data.id}
                </text>
            </g>
        );
    }
}

export default App;